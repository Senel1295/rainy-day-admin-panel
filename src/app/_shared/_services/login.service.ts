import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private code: any;
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getLogedUser(): Observable<any> {
    return new Observable(obs => {
      this.code == 1 ? obs.next(this.code) : obs.error(this.code);
      obs.complete();
    })
  }

  autheticate(login: any): Observable<any> {
    let url = this.baseUrl + '/api/login';
    console.log(url);
    return new Observable(obs => {
      console.log(login)
      this.http.post(url, login).subscribe(res => {
        console.log(res)
        this.code = res['code'];
        if(this.code!=0){
          obs.next(this.code);
          obs.complete();
        } else {
          obs.error(this.code);
          obs.complete();
        }
      }, error => {
        this.code = 1;
        obs.error(this.code);
        obs.complete();
      });
    });
  }

  private handleError(error: HttpErrorResponse) {
    return Observable.throwError(error.message || 'Server error');
  }

}