import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
  })


  export class SchedulService {
    private code: any;
    private uid: any;
    baseUrl = environment.baseUrl;
  
    constructor(private http: HttpClient) { }
  

  
    getSchedulByStatus(schedul:any): Observable<any> {
      let scheduleUrl = this.baseUrl + '/api/getSchedules';
      return new Observable(obs => {
        this.http.post(scheduleUrl, schedul).subscribe(res => {
          this.code = res['code'];
          if(this.code!=0){
            obs.next(res);
            obs.complete();
          } else {
            obs.error(this.code);
            obs.complete();
          }
        }, error => {
          this.code = 0;
          obs.error(this.code);
          obs.complete();
        });
      });
    }
  
    updateSchedul(schedul:any): Observable<any> {
      let updateUrl = this.baseUrl + '/api/updateSchedules';
      return new Observable(obs => {
        this.http.post(updateUrl, schedul).subscribe(res => {
          this.code = res['code'];
          if(this.code!=0){
            obs.next(res);
            obs.complete();
          } else {
            obs.error(this.code);
            obs.complete();
          }
        }, error => {
          this.code = 0;
          obs.error(this.code);
          obs.complete();
        });
      });
    }

    addTips(tips:any): Observable<any> {
      let tipsUrl = this.baseUrl + '/api/addTip';
      return new Observable(obs => {
        this.http.post(tipsUrl, tips).subscribe(res => {
          this.code = res['code'];
          if(this.code!=0){
            obs.next(res);
            obs.complete();
          } else {
            obs.error(this.code);
            obs.complete();
          }
        }, error => {
          this.code = 0;
          obs.error(this.code);
          obs.complete();
        });
      });
    }

  }



































