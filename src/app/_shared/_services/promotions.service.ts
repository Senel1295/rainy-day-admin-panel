import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {
  private code: any;
  private uid: any;
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getPromotions(): Observable<any> {
    let getUrl = this.baseUrl + '/api/getDefaultPromotions';
    return new Observable(obs => {
      this.http.get(getUrl).subscribe(res => {
        this.code = res['code'];
        if(this.code!=0){
          obs.next(res);
          obs.complete();
        } else {
          obs.error(this.code);
          obs.complete();
        }
      }, error => {
        this.code = 0;
        obs.error(this.code);
        obs.complete();
      });
    });
  }

  addPromotion(promotion:any): Observable<any> {
    let postUrl = this.baseUrl + '/api/addPromotion';
    return new Observable(obs => {
      this.http.post(postUrl, promotion).subscribe(res => {
        this.code = res['code'];
        if(this.code!=0){
          obs.next(res);
          obs.complete();
        } else {
          obs.error(this.code);
          obs.complete();
        }
      }, error => {
        this.code = 0;
        obs.error(this.code);
        obs.complete();
      });
    });
  }

  private handleError(error: HttpErrorResponse) {
    return Observable.throwError(error.message || 'Server error');
  }

}