import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private code: any;
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getLocations(): Observable<any> {
    let url = this.baseUrl + '/api/getLocations';
    return new Observable(obs => {
      this.http.get(url).subscribe(res => {
        this.code = res['code'];
        if(this.code!=0){
          obs.next(res);
          obs.complete();
        } else {
          obs.error(this.code);
          obs.complete();
        }
      }, error => {
        this.code = 0;
        obs.error(this.code);
        obs.complete();
      });
    });
  }

  private handleError(error: HttpErrorResponse) {
    return Observable.throwError(error.message || 'Server error');
  }

}