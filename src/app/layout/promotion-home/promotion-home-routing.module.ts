import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PromotionHomeComponent } from "./promotion-home.component";

const routes: Routes = [{path: '', component: PromotionHomeComponent}]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class promotionHomeRoutingModule {}