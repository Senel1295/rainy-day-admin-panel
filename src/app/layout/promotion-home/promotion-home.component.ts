import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocationService } from 'src/app/_shared/_services/location.service';
import { PromotionsService } from 'src/app/_shared/_services/promotions.service';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-promotion-home',
  templateUrl: './promotion-home.component.html',
  styleUrls: ['./promotion-home.component.scss']
})
export class PromotionHomeComponent implements OnInit {
  promotionFormGroup: FormGroup;
  promotions: any[] = [];
  locations: any[] = [];
  promotionUrl: any;
  promotionName: any;
  locationId: any;
  selected = 'Select a location';

  isVisibleSuccess: boolean;
  isVisibleError:boolean
  isLoading:boolean;

  constructor(  private promotionService: PromotionsService, 
                private locationService: LocationService,
                private toastr: ToastrService,
                private fb: FormBuilder
                ) { }


  ngOnInit() {
    this.initForm();
    this.getLocation();
    this.getPromotion();
  };

  initForm() {
    this.promotionFormGroup = this.fb.group({
      location: ['', Validators.compose([Validators.required])]
    });
  }

  getPromotion() {    
    this.promotionService.getPromotions().subscribe(res => {
      this.promotions = res.data;
      this.promotionUrl = this.promotions[0].image_url;
      this.promotionName = this.promotions[0].description;
    })
  }

  seclectedImage(promotionId: any, promotionName: any) {
    this.promotionUrl = promotionId;
    this.promotionName = promotionName;
  }

  getLocation() {
    console.log('getLocation')
    this.locationService.getLocations().subscribe(res => {
      this.locations = res.data;
      console.log("location:")
      console.log(this.locations);
    });
  }

  selectChangeHandler(event: any) {
    this.selected = event.target.value;
    console.log(this.selected);
  }

  addPromotion() {
    this.isLoading = true;
    var promotion = {
      "promotion_name": this.promotionName,
      "image_url": this.promotionUrl,
      "lid": this.selected,
      "promotion_type": 2
    }

    this.promotionService.addPromotion(promotion).subscribe(res => {
      if (res.code == 1) {
        this.toastr.success(res.message);
        this.isLoading = false;
        //this.isVisibleSuccess = true;
        // alert("Successfully published");
      } else {
        this.toastr.error(res.message);
        this.isLoading = false;
        //this.isVisibleError = true
        // alert("Please enter all fields");
      }
    }, error => {
      this.toastr.error("Please enter all fields!");
      this.isLoading = false;
      //this.isVisibleError = true
      // alert("Please enter all fields");
    });
  }

  closeAlert(){
    this.isVisibleSuccess = false;
    this.isVisibleError = false;
  }

}