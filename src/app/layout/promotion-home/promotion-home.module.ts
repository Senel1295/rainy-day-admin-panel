import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionHomeComponent } from './promotion-home.component';
import { promotionHomeRoutingModule } from './promotion-home-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({

  imports: [
    CommonModule,
    promotionHomeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  
  declarations: [
    PromotionHomeComponent
  ]
  
})
export class PromotionHomeModule { }
