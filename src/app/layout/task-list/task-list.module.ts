import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { TaskListComponent } from './task-list.component';
import { TaskListRoutingModule } from './task-list-routing.module';

@NgModule({

  imports: [
    CommonModule,
    TaskListRoutingModule,
    NgbModule
  ],

  declarations: [
    TaskListComponent,
  ],

})
export class TaskListModule { }
