import { Component, OnInit, ɵConsole, ViewChild, TemplateRef } from '@angular/core';
import { SchedulService } from 'src/app/_shared/_services/schedul.service'


import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})

export class TaskListComponent implements OnInit {

  @ViewChild('editModal') editModal : TemplateRef<any>;


  tasks: any[] = [];
  taskCount: number = 0;
  tabStatus = 'pending';u
  openedTask: any;
  subTaskCount: number = 0;

  constructor(private schedulService: SchedulService,   private modalService: NgbModal, private toastr: ToastrService) { }

  ngOnInit() {
    this.getScheduls();
  }

  clickTab(status: any) {
    this.tasks = [];
    this.taskCount = 0;

    this.tabStatus = status;
    this.getScheduls();
  }

  getScheduls() {
    var status = {
      "status": this.tabStatus
    }
    this.schedulService.getSchedulByStatus(status).subscribe(res => {
      this.tasks = res.data;
      this.taskCount = this.tasks.length;
    }, error => {});
  }

  openTask(obj: any) {
    this.openedTask = obj;
    this.subTaskCount = this.openedTask.tasks.length;
    this.modalService.open(this.editModal,{ size: 'lg' });
  }


  taskUpdate(id: any, stat: any) {
    var status = {
      "schedule_id": id,
      "status": stat
    }
    this.schedulService.updateSchedul(status).subscribe(res => {
      if (res.code == 1) {
        this.toastr.success("Successfully updated!");
      }
      else {
        this.toastr.error(res.message);
      }
    }, error => { 
      this.toastr.error(error);
    });

    this.modalService.dismissAll();
    this.getScheduls();
  }

}
