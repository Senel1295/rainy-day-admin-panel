import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionAddComponent } from './promotion-add.component';
import { PromotionAddRoutingModule } from './promotion-add-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  
  imports: [
    CommonModule,
    PromotionAddRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],

  declarations: [
    PromotionAddComponent
  ],
  
})
export class PromotionAddModule { }
