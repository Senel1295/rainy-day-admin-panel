import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PromotionAddComponent } from "./promotion-add.component";


const routes: Routes = [{path: '', component: PromotionAddComponent}]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PromotionAddRoutingModule {}