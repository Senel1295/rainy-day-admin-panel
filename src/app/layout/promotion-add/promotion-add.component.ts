import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { LocationService } from 'src/app/_shared/_services/location.service';
import { PromotionsService } from 'src/app/_shared/_services/promotions.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-promotion-add',
  templateUrl: './promotion-add.component.html',
  styleUrls: ['./promotion-add.component.scss']
})
export class PromotionAddComponent implements OnInit {
  
  promotionFormGroup: FormGroup;
  selectedFile: File;
  url:any;
  locations: any[] = [];
  private isVisible: boolean;
  isVisibleSuccess: boolean;
  isVisibleError:boolean;
  isLoading:boolean;


  constructor(private promotionService:PromotionsService,private locationService:LocationService, private fb: FormBuilder, private toastr: ToastrService) { }


  ngOnInit() {
    this.isVisible = true;
    this.initForm();
  }

  initForm() {
    this.promotionFormGroup = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      location:['', Validators.compose([Validators.required])],
      discount:['', Validators.compose([Validators.max(100), Validators.min(0)])],
      promoimage: ['', Validators.compose([Validators.required])],
    }, {validator: this.discountValidator});
    this.getLocation();
  }

  discountValidator(c: AbstractControl): { invalid: boolean } {
    if (c.get('discount').value == null ||  c.get('discount').value == 0 ) {
        return {invalid: true};
    }
  }

  processFile(imageInput: any) {
    
    this.selectedFile = imageInput.target.files[0];
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    reader.readAsDataURL(imageInput.target.files[0]);
  }

  getLocation() {
    this.locationService.getLocations().subscribe(res=>{
      this.locations = res.data;
    }, error => {

    });
  }


  publish(){
    const formData: any = new FormData();
    let promotion = this.promotionFormGroup.value;
    formData.append("image",this.selectedFile,this.promotionFormGroup.value);
    formData.append("promotion_name", promotion.title)
    formData.append("promotion_desc", promotion.description)
    formData.append("promotion_discount", promotion.discount)
    formData.append("lid", promotion.location)

    console.log(formData.toString())
    this.isLoading = true;
    this.promotionService.addPromotion(formData).subscribe(res =>{
      console.log('response.........')
      console.log(res)
      if(res.code==1){
        // alert("Successfully published");
        //this.isVisibleSuccess=true;
        this.toastr.success(res.message);
        this.isLoading = false;
      } else {
        // alert("Please enter all fields");
        this.toastr.error(res.message);
        this.isLoading = false;;
        //this.isVisibleError=true;
      }
    }, error=>{
      // alert("Please enter all fields");
      //this.isVisibleError=true
      this.toastr.error("Please enter all fields!");
      this.isLoading = false
    });
  }


  closeAlert(){
    this.isVisibleSuccess = false;
    this.isVisibleError = false;
  }

}
