import { Component, OnInit } from '@angular/core';
import { LoginService } from '../_shared/_services/login.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  public isVisible: boolean;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.loginService.getLogedUser().subscribe(res => {
      console.log("nav checker")
      console.log(res)
      if(res.code==1){
        this.isVisible = true;
      }
      else{
        this.isVisible = true;
      }
    }, err => {
      console.log(err)
      this.isVisible = false;
    });
  }
}
