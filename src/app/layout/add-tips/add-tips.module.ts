import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddTipsComponent } from './add-tips.component';
import { AddTipsRoutingModule } from './add-tips-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  
  imports: [
    CommonModule,
    AddTipsRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],

  declarations: [
    AddTipsComponent
  ],
  
})
export class AddTipsModule { }
