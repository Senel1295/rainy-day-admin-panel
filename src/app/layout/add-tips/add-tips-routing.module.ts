import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddTipsComponent } from "./add-tips.component";


const routes: Routes = [{path: '', component: AddTipsComponent}]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AddTipsRoutingModule {}