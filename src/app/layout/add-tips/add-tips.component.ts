import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { SchedulService } from 'src/app/_shared/_services/schedul.service';

@Component({
  selector: 'app-add-tips',
  templateUrl: './add-tips.component.html',
  styleUrls: ['./add-tips.component.scss']
})
export class AddTipsComponent implements OnInit {
  tipsFormGroup: FormGroup;
  private isVisible: boolean;
  isVisibleSuccess: boolean;
  isVisibleError:boolean

  constructor(private scheduleService:SchedulService, private fb: FormBuilder) { }

  ngOnInit() {
    this.isVisible = true;
    this.initForm();
  }

  initForm() {
    this.tipsFormGroup = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      tip: ['', Validators.compose([Validators.required])],
    });
  }

  publish(){
    const formData: any = new FormData();
    let tips = this.tipsFormGroup.value;
    console.log(tips)

    this.scheduleService.addTips(tips).subscribe(res =>{
      if(res.code==1){
        // alert("Successfully published");
        this.isVisibleSuccess=true;
      } else {
        // alert("Please enter all fields");
        this.isVisibleError=true;
      }
    }, error=>{
      // alert("Please enter all fields");
      this.isVisibleError=true
    });
  }
  closeAlert(){
    this.isVisibleSuccess = false;
    this.isVisibleError = false;
  }
  }

