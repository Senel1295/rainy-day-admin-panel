import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';

import { LayoutHeaderComponent } from './layout-header/layout-header.component';
import { LayoutFooterComponent } from './layout-footer/layout-footer.component';
import { AddTipsComponent } from './add-tips/add-tips.component';


@NgModule({
  
  imports: [
    CommonModule,
    LayoutRoutingModule,
    RouterModule
  ],

  declarations: [
    LayoutComponent,
    LayoutHeaderComponent,
    LayoutFooterComponent
  ]  

})
export class LayoutModule { }
