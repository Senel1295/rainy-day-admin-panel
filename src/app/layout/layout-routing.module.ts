import { NgModule } from "@angular/core";
import {  RouterModule, Routes } from "@angular/router";

import { LayoutComponent } from "./layout.component";

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent, 
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'prefix' },
            { path: 'home', loadChildren: './promotion-home/promotion-home.module#PromotionHomeModule'},
            { path: 'add', loadChildren: './promotion-add/promotion-add.module#PromotionAddModule'},
            { path: 'tasks', loadChildren: './task-list/task-list.module#TaskListModule'},
            { path: 'addtips', loadChildren: './add-tips/add-tips.module#AddTipsModule'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})


export class LayoutRoutingModule {}