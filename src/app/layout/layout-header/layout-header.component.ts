import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'layout-header',
  templateUrl: './layout-header.component.html',
  styleUrls: ['./layout-header.component.scss']
})
export class LayoutHeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  logOut() {

    localStorage.removeItem('isLoggedin');
    this.router.navigate(['/login']);
  }
}
