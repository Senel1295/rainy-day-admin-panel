import { Observable } from "rxjs";
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from "@angular/router";
import { LoginService } from "../_shared/_services/login.service";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class PermissionGuard implements CanActivate {

    constructor(
        private loginService: LoginService,
        private router: Router) {
    }


    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | Promise<boolean> | boolean {
        return new Observable(obs => {
            if (localStorage.getItem('isLoggedin')) {
                obs.next(true);
                obs.complete();
            } else {
                obs.next(false);
                obs.complete();
                this.router.navigate(['/login']);
            }

            // this.loginService.getLogedUser().subscribe(res => {
            //     obs.next(true);
            //     obs.complete();
            // }, error => {
            //     obs.next(false);
            //     obs.complete();
            //     this.router.navigate(['/login']);
            // });

        });
    }

}