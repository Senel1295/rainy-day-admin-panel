import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../_shared/_services/login.service';
import { RouterOutlet, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('alert') alert: ElementRef;
  private message;
  loginFormGroup: FormGroup
  public isVisible: boolean;
  

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private titleService:Title) {
      this.titleService.setTitle("Rainyday");
     }

    closeAlert() {
      this.isVisible = false;
    }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.loginFormGroup = this.fb.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      user_type:1
    });
  }

  login() {
    let user = this.loginFormGroup.value;
    user.device_id = '';
    user.device_type = "web";
    user.token = "AiZXCDfaADAzxAscdasdAdascxzcasdaAA"
    this.loginService.autheticate(user).subscribe(res => {


      localStorage.setItem('isLoggedin', 'true');
      this.router.navigate(['/home']);
      this.message = res.message;

    }, error => { 

      this.message = error.message;
      this.isVisible = true
    });
  }

}
