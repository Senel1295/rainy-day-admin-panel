import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard } from './_guard/permission.guard';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  { path: '', loadChildren: './layout/layout.module#LayoutModule', canActivate: [PermissionGuard]},
  { path: 'login', loadChildren: './login/login.module#LoginModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
